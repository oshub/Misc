#!/bin/bash
## chenxiaohong@mail.com
## zhuguanghong
## 通过sftp获取远程服务器指定文件。
#
# 属性声明
# 线程数量控制
SESSION_LIMIT=10
# 进程ID
SESSION_PID=()
# 服务器信息文件
SERVER_FILE=servers.txt
# 默认用户名
DEF_USER='collect'
# 默认密码
DEF_PSWD=''
# 默认远程目录
DEF_WORK='/collect/disk/log/'
# END

execute() {
  lines=`wc -l < $SERVER_FILE | xargs || echo 0`;

  log "待处理服务器数量$lines";

  while read url; do
      # 去除左右空白符
      url=`echo $url | sed -e 's/^ *//g' -e 's/ *$//g'`
      if [ ! -z $url ]; then
        #确认当前线程数量没有超过控制
        while [ ${#SESSION_PID[@]} -ge $SESSION_LIMIT ]; do 
          for pid_idx in ${!SESSION_PID[*]}; do
            pid_val=${SESSION_PID[$pid_idx]}
            kill -s 0 $pid_val &>/dev/null || unset SESSION_PID[$pid_idx]
          done
          SESSION_PID=("${SESSION_PID[@]}")
        done;

        IFS=',' read -ra infos <<< "$url";

        user="$DEF_USER";
        pswd="$DEF_PSWD";
        work="$DEF_WORK";
        file="disk_${infos[0]}.log";

        if [ ! -z "${infos[2]}" ]; then 
        	user="${infos[2]}";
        fi
        if [ ! -z "${infos[3]}" ]; then 
        	pswd="${infos[3]}";
        fi
        if [ ! -z "${infos[4]}" ]; then 
        	work="${infos[4]}";
        fi

        proces_download ${infos[1]} $user $pswd $work $file &
        #process $1 $url &
        SESSION_PID=("${SESSION_PID[@]}" "$!")
      fi
    done < $SERVER_FILE
}

# DOWNLOAD
proces_download () {
  lftp -u $2,$3 sftp://$1 <<EOF
    # 自动覆盖已存在文件
    set xfer:clobber on
    cd $4
    pget $5
    bye
EOF
}

log() {
  info="[`date +'%Y-%m-%d %T'`] $1";
  printf "$info\n";
  # echo "$info" >> "debug.log"
}

execute;

log "DONE.";

exit 0;