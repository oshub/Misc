options (errors=-1)
LOAD DATA
// characterset zhs16gbk // 可以指定编码
INTO TABLE t_ips
// 数据追加到表
APPEND
// 字段分隔符
FIELDS TERMINATED BY ',' optionally enclosed by '"'
// 字段声明
TRAILING NULLCOLS
(
monitor,
url,
ip,
date,
id "sqlldr_insert.nextval"
)
