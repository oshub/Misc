#!/bin/bash
# chenxiaohong@mail.com
# DIG, PING 数据采集脚本
# 核心文件，功能实现

# Monitor Number
MONITOR=0
# Url file
FILES="urls.txt"
# Result file
RESULTFILE="data/result.data"
# 线程数量控制
SESSION_LIMIT=10
# 进程ID
SESSION_PID=()

execute () {
  log "当前监测点编号:$MONITOR";
  # 确定文件名称
  RESULTFILE="data/${MONITOR}_`echo $1 | tr a-z A-Z`_`date +'%Y-%m-%d'`.data"
  if [ ! -d "${RESULT_FILE%/*}" ]; then
    mkdir -p -- "${RESULTFILE%/*}"
  fi
  # 备份旧文件
  if [ -f "$RESULTFILE" ]; then
    mv $RESULTFILE "${RESULTFILE}_`date +%H:%M`.bak"
  fi

  log "等待处理数据文件:$FILES";
   # 数据文件行数
  lines=0;
  for file in $FILES; do
    line=`wc -l < $file`
    lines=$((lines + line))
    # lines =$lines + $(wc -l < $file || echo 0)
  done
  # lines=`wc -l < $FILES || echo 0`;
  log "当前待处理url数量:$lines";

  log "正在处理采集任务..";
  for file in $FILES; do
    while read url; do
      # 去除左右空白符
      url=`echo $url | sed -e 's/^ *//g' -e 's/ *$//g'`
      if [ ! -z $url ]; then
        #确认当前线程数量没有超过控制
        while [ ${#SESSION_PID[@]} -ge $SESSION_LIMIT ]; do 
          for pid_idx in ${!SESSION_PID[*]}; do
            pid_val=${SESSION_PID[$pid_idx]}
            kill -s 0 $pid_val &>/dev/null || unset SESSION_PID[$pid_idx]
          done
          SESSION_PID=("${SESSION_PID[@]}")
        done;
        process $1 $url &
        SESSION_PID=("${SESSION_PID[@]}" "$!")
      fi
    done < $file
  done

  log "等待任务执行完成.."
  wait; # 等待采集线程完成

  log "正在上传数据..";
  proces_upload $RESULTFILE;
}

process () {
  case $1 in
    "dig")
      proces_dig $2;;
    "ping")
      proces_ping $2;;
    *)
      printf "Argument error.\n";;
  esac
}

# DIG
proces_dig () {
  ips=`dig $1 | grep -p "IN\s*A" | awk '{if($5 -n)print $5}'`;
  for ip in $ips; do
    echo "$MONITOR,$1,$ip,`date +'%Y-%m-%d %H:%M'`" >> $RESULTFILE
  done
}

# PING
proces_ping () {
  ip=`ping -c 1 $1 | head -n 1 | cut -d ' ' -f 2,3 | cut -d ':' -f 1 | awk '{gsub(/[()]/, "", $2);print $1 "," $2}'`
  echo "$MONITOR,$1,$ip,`date +'%Y-%m-%d %H:%M'`" >> $RESULTFILE
}

# UPLOAD
proces_upload () {
  USER="ngoss";
  PSWD="9ol.0p;/";
  HOST="192.168.1.15"
  WORKDIR="/ngoss/data"

  lftp -u ${USER},${PSWD} sftp://$HOST <<EOF
    cd $WORKDIR
    mput $*

    bye
EOF
}

log() {
  info="[`date +'%Y-%m-%d %T'`] $1";
  printf "$info\n";
  # echo "$info" >> "debug.log"
}

# Argument Check
if [ ! -z $2 ]; then
  MONITOR=$2;
fi

if [ ! -z $3 ]; then
  FILES=${@:3};
fi

case $1 in
  "dig" | "ping")
    execute $1;;
  "upload")
    proces_upload ${@:2};;
  *)
    echo "usage: $0 dig|ping [monitor]"
    exit 1;;
esac

wait;

log "DONE";

exit 0
